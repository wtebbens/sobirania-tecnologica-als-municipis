# Propostes sobre Programari Lliure

## Objectius

Tres nivells:

  1. No hostilitat cap al programari lliure
  2. Utilitzar el màxim de programari lliure per part de l'ajuntament
  3. Promoure l'ús de programari lliure a la societat

Objectius concrets:

  * (1.a) Evitar cap dificultat artificial en l'ús de programari lliure per part de la ciutadania
  * (1.b) No obligar mai la ciutadania a utilitzar tecnologies d'un fabricant concret, o serveis que espiïn, que obliguin a donar més dades de les necessàries, o que utilitzin l'activitat de les usuàries per fer negoci.
  * (2.a) Aconseguir que tot el codi finançat per la gent estigui a disposició de la gent.
  * (2.b) Que l'administració local tingui ple control i capacitat de decisió sobre quines tecnologies implanta, com gestiona les dades que processa i quins algorismes hi executa.
  * (2.c) Reduir l'obsolescència de les tecnologies implantades i la generació de residus tecnològics.
  * (2.d) Compartir coneixement i costos entre ajuntaments i amb entitats de l'economia social a l'hora d'implantar sistemes tecnològics. No partir de zero en cada sistema implantat.
  * (2.e) Potenciar l'ús de programari lliure per part dels ajuntaments.
  * (3.a) Promoure la innovació i la creació d'un teixit tecnològic local basat en tecnologies lliures
  * (3.b) Invertir recursos públics en mantenir, cuidar i fer créixer el procomú digital.

## Proposta programàtica

  * Totes les webs, apps o plataformes tecnològiques municipals, han de funcionar perfectament sobre plataformes lliures (navegadors, escriptoris, sistemes operatius), sense requerir la instal·lació de cap component no lliure.
  * Basar els nous desenvolupaments en tecnologies lliures, sempre que sigui possible. Publicar-los sota una llicència lliure, i aïllar aquelles parts que transitòriament hagin de fer ús de tecnologies privatives en forma de components substituïbles amb interfícies ben documentades. TODO: desenvolupaments tecnològics i estudis tècnics en general (Muriel).
  * La compra de maquinari ha de tenir com a prioritat que aquest pugui funcionar sense inconvenients sobre programari lliure (TODO: firmware, BIOS).
  * Documentar els desenvolupaments publicats per tal que qualsevol persona o entitat amb els coneixements tècnics necessaris els puguin reutilitzar.
  * Contribuir totes les millores i modificacions realitzades a productes de programari lliure existents al projecte original, procurant que siguin acceptades abans de fer-les servir (upstream first).
  * Respectar les pràctiques i els codis de conducta, explícits o no, de les comunitats en que es participa.
  * Crear mecanismes de coordinació entre administracions i entitats de l'economia social que permetin mancomunar desenvolupaments. TODO: Fomentar processos de disseny, desenvolupament i implementació de codi que resolguin necessitats comunes, mitjançant la cooperació, la coproducció i la compartició de coneixements i recursos.
  * TODO: alguna cosa sobre formació del personal administració.
  * TODO: alguna cosa sobre implantació de PL als ajuntaments (GNU/Linux, Libreoffice, objectiu 2.e).

### Experiències inspiradores

  * Hi ha multitud d'organitzacions fent servir Linux i programari lliure (llista no actualitzada): https://en.wikipedia.org/wiki/List_of_Linux_adopters
  * Múnic: https://www.kdab.com/the-limux-desktop-and-the-city-of-munich/ (beneficis guanyats amb coses que ha fet aquesta consultora). TODO: mirar com presentar-ho, és un cas controvertit.
  * TODO: UK libreoffice, Austràlia, Nova Zelanda.
  * Projecte Decidim.org: s'utilitza ja a molts municipis
  * Migració a PL a l'Ajuntament de Saragossa

### Referències

  * Campanya Public Money, Public Code: https://publiccode.eu/ca/
  * Projecte GNU: https://www.gnu.org/philosophy/government-free-software.en.html
  * Barcelona: https://ajuntament.barcelona.cat/digital/ca/documentacio

==== Justificació dels objectius i la proposta programàtica ====

TODO.
