# Guia per l'apoderament tecnològic als municipis

## Intro
La Guia per l'apoderament tecnològic als municipis és un document que pretén avançar en la Sobirania Tecnològica a través de la implementació de polítiques municipals concretes. La voluntat d'aquest document és el d'impregnar de sobirania tecnologica els programes dels partits polítics que concorreran a les eleccions municipals del 2019, no només dedicant un apartat especific dels seus programes a les propostes que farem a continuació, si no aplicant al programa sencer una nova mirada.

Tot i la clara aposta d'aquesta guia per un canvi en les polítiques municipals, el procés per avançar cap a aquesta sobirania, no s'ha d'entendre com un procés exclusivament liderat per l'administració pública, sino com un fet compartit entre els diferents agents d'un territori. És per això que entenem aquesta guia com un full de ruta que ens ha de servir a les diferents entitats que treballem aquest tema, per poder fer un seguiment i revisió de la nostra pròpia feina.


## Argumentari

La tecnologia, o l'activitat tecnològica d'una regió, influeix en el progrés econòmic i social d'aquesta.  És per això que calen unes polítiques municipals que donin als municipis la capacitat de ser protagonistes de la seva activitat tecnològica i que no es limitin, com passa en l'actualitat, només a potenciar l'evolució tecnològica orientada per l'interès privat de les empreses líders del sector. Aquest protagonisme ha de servir per fer créixer una tecnologia oberta, de fàcil accés i sota control democràtic.

A més, la tecnologia ha inundat tots els aspectes de la vida diària de les persones, facilitant o limitant les llibertats personals. Per això. cal que l'administració no se'n desentengui i no només reguli adequadament la l'us de les tecnologies en aquests àmbits, sinó que també sigui exemplar vetllant per les llibertats dels ciutadans.

No s'ha de confondre el fet de ser sobirans tecnològicament, amb una administració pública que controli la tecnologia, sino tot el contrari. La sobirania tecnològica, s'ha de basar en tecnologies lliures: de lliure accés, ús, estudi i rèplica, que evitin, pricisament, el control tecnològic. A més a més, la naturalesa d'aquestes tecnologies, propicia que el seu disseny i desenvolupament es faci a través de xarxes d'agents on es poden combinar les capacitats tècniques amb el coneixement de les necessitats del territori, i on no recaigui únicament sobre l'administació pública la responsabilitat de fer evolucionar la tecnologia.


## XXX mesures per a l'apoderament tecnològic als muncipipis

### Infraestructura de telecomunicacions
La infraestructura de telecomunicacions és tot aquell cablejat i maquinari que fa possible que la informació viatgi d'un lloc a un altre. És el pilar d'Internet. Actualment tenim infraestructures públiques, privades i procomunes, segons quin el seu propietari i quin model de gestió té.
Les decisions que es prenen relatives a la regulació, evolució o ampliació d'aquesta infraestructura han sigut objecte de fortes tensions entre diferents interessos privats que busquen controlar la xarxa i la  informació que hi circula per poder extreure'n un benefici. També hem pogut veure com el control d'aquesta xarxa s'ha utilitzat dins de campanyes de censura.


#### Objectius generals
* Tendir cap a la universalització i sobirania de les xarxes de telecomunicacions.
* Impulsar que el desplegament del 5G es faci a través d'una xarxa compartida pels diferents operadors.
* Potenciar les operadores de proximitat. Evitant el monopoli de les grans operadores.
* Incrementar el control públic dels operadors.
* Potenciar l'ús neutre de les xarxes de telecomunicacions.
* Construir una xarxa de dades d’Internet de les coses oberta, lliure i neutral. creada col·lectivament des de baix.

> es podrien unificar els diversos punts en un o dos? Tenir molts punts fan més feixuga la lectura i li treuen protagonisme als punts programàtics

#### Proposta programàtica

##### Desplegament de nova infraestructura
1. Proporcionar accés real a la ciutadania i a la societat en general a una oferta assequible i variada de serveis de telecomunicacions de la màxima qualitat, capacitat i neutral.
2. Promoure ordenances municipals de desplegament de nova infraestructura com la proposada per Guifi.net,
> [falta explicar que proposa guifinet amb una frase]
3. Afavorir les xarxes comunals i obertes.
> [Falta una frase per explicar com es pot afavorir. potser amb un exemple ja està...]

##### Desplegament de la xarxa mòbil de cinquena generació
4. Impulsar que el desplegament del 5G es faci a través d'una xarxa compartida pels diferents operadors.
5. Facilitar la implementació d'antenes en edificis públics.

##### Contractació de serveis de telecomunicacions
6. Contractar serveis a companyies de telecomunicacions que no discriminin el trànsit, el filtrin o l'interrompin, com en el cas del referendum d'autodeterminació de l'1 d'octubre de 2017.

##### Internet dels Objectes
7. Promoure les iniciatives de xarxes internet dels objectes oberta, lliure i neutral, [mitjançant ...]
8. Contribuïr a una arquitectura d'internet dels objectes per les ciutats que sigui oberta i interoperable.


### Programari Lliure
Quan un programa informàtic és pot utilitzar sense cap restricció, modificar segons les pròpies necessitats, compartir amb qui també el necessita, i estudiar per entendre com funciona, llavors l'anomenem lliure. EL programari lliure permet que, mitjançant la cooperació, la coproducció i la compartició de coneixements i recursos, es puguin resoldre necessitats comunes. També evita la dependència a l'empresa propietària del programa i possibilita verificar -en termes de seguretat- que el programa només fa el que ha de fer.

#### objectiu general de l'àmbit
* No hostilitat cap al programari lliure
  * Evitar cap dificultat artificial en l'ús de programari lliure per part de la ciutadania
  * No obligar mai la ciutadania a utilitzar tecnologies d'un fabricant concret, o serveis que espiïn, que obliguin a donar més dades de les necessàries, o que utilitzin l'activitat de les usuàries per fer negoci.
* Utilitzar el màxim de programari lliure per part de l'ajuntament
  * Aconseguir que tot el codi finançat per la gent estigui a disposició de la gent.
  * Que l'administració local tingui ple control i capacitat de decisió sobre quines tecnologies implanta, com gestiona les dades que processa i quins algorismes hi executa.
  * Reduir l'obsolescència de les tecnologies implantades i la generació de residus tecnològics.
  * Compartir coneixement i costos entre ajuntaments i amb entitats de l'economia social a l'hora d'implantar sistemes tecnològics. No partir de zero en cada sistema implantat.
  * Potenciar l'ús de programari lliure per part dels ajuntaments.
* Promoure l'ús de programari lliure a la societat    
  * Promoure la innovació i la creació d'un teixit tecnològic local basat en tecnologies lliures
  * Invertir recursos públics en mantenir, cuidar i fer créixer el procomú digital.
> es podrien unificar els diversos punts en un o dos? Tenir molts punts fa més feixuga la lectura i li treuen protagonisme als punts programàtics

#### Proposta programàtica
1. Vetllar per a que totes les webs, apps o plataformes tecnològiques municipals, funcionin perfectament sobre plataformes lliures (navegadors, escriptoris, sistemes operatius), sense requerir la instal·lació de cap component no lliure.
1. Basar els nous desenvolupaments en tecnologies lliures, sempre que sigui possible. Publicar-los sota una llicència lliure, i aïllar aquelles parts que transitòriament hagin de fer ús de tecnologies privatives en forma de components substituïbles amb interfícies ben documentades. TODO: desenvolupaments tecnològics i estudis tècnics en general (Muriel).
1. Garantir que tot el maquinari es compripugui funcionar sense inconvenients sobre programari lliure (TODO: firmware, BIOS).
1. Documentar els desenvolupaments publicats per tal que qualsevol persona o entitat amb els coneixements tècnics necessaris els puguin reutilitzar.
1. Contribuir totes les millores i modificacions realitzades a productes de programari lliure existents al projecte original, procurant que siguin acceptades abans de fer-les servir (upstream first).
1. Respectar les pràctiques i els codis de conducta, explícits o no, de les comunitats en que es participa.
1. Crear mecanismes de coordinació entre administracions i entitats de l'economia social que permetin mancomunar desenvolupaments. TODO: Fomentar processos de disseny, desenvolupament i implementació de codi que resolguin necessitats comunes, mitjançant la cooperació, la coproducció i la compartició de coneixements i recursos.
1. TODO: alguna cosa sobre formació del personal administració.
1. TODO: alguna cosa sobre implantació de PL als ajuntaments (GNU/Linux, Libreoffice, objectiu 2.e).

### Política de dades
> Falta introducció entenedora

#### objectiu general de l'àmbit
* Fer creixer les col·leccions de dades de lliure accés i facil explotació.
* Fer més transparent l'adminstració.
* Garantir la privacitat de les dades de caràcter personal.
* Adminstrar les dades personals de manera més conscient, reduint els riscos derivats de la seva explotació.
* Establir criteris ètics per a la explotació de les dades de caràcter personal.
> es podrien unificar els diversos punts en un o dos? Tenir molts punts fa més feixuga la lectura i li treuen protagonisme als punts programàtics

#### Proposta programàtica

##### Open data per defecte
1. Publicar per defecte sota llicències lliures i formats explotables tota la informació pública generada o gestionada per les entitatats municipals; des dels pressupostos municipals fins als temps dels semàfors
1. Publicar les dades generades per a la realització d'estudis finançats amb diners públics, sota llicencies lliures i formats explotables, de tal manera que es puguin comprovar els estudis o fer-ne de derivats.

##### Privacitat de les dades de caràcter personal
1. Administrar les dades de caràcter personal únicament dins de l'administració i seguint estrictes criteris de seguretat, garantint els mitjans tècnics i els coneixements per gestionar aquest tipus de dades sense dependre de tercers.
1. Limitar la recollida de dades de caràcter personal a aquells casos en què prèviament s'han establert les finalitats concretes per a les que seran utilitzades, minimitzant, així, la quantitat de dades personals que es recullen. En aquest sentit, si per exemple es recullen dades per entendre com es mouen les persones dins d'una ciutat i poder, d'aquesta manera,millorar el transport públic; aquestes dades no es podran utilitzar per estudiar els seus hàbits de consum.

### Democratització de la tecnologia
> Falta introducció entenedora

#### objectiu general de l'àmbit
* Posar a l’abast de tothom les innovacions tecnològiques que l'administració incorpora en la seva acció municipal.
* Pal·liar la distància creada a partir dels diferents usos tecnològics per part dels diferents sectors de la societat (escletxa digital), tant per accés com per coneixement.
* Vetllar per un accés no discriminador a tràmits i accions digitals de l'administració.
* Acompanyar tota acció que impliqui una interacció digital amb la ciutadania de la formació necessària per a que tot tècnic municipal pugui guiar a la ciutadania.
* Promoure una relació conscient i crítica de la ciutadania amb la tecnologia.
> es podrien unificar els diversos punts en un o dos? Tenir molts punts fa més feixuga la lectura i li treuen protagonisme als punts programàtics

#### Proposta programàtica
1. Posar en marxa programes de formació i capacitació digital per a la ciutadania, així com reforçar els existents.
1. Promoure espais de connexió i ús tecnològic que ofereixin eines i recursos de manera gratuïta, així com dotar de recursos als existents (biblioteques, centres cívics o telecentres).
1. Oferir programes pedagògics en coordinació amb centres educatius de diferents nivells que permetin abordar com treballar la tecnologia de manera inclusiva i respectuosa amb la ciutadania.
1. Oferir formació tecnològica (inicial i avançada) a col·lectius amb risc d’exclusió social per motius socioeconòmics, en temàtiques d’innovació tecnològica, en coordinació amb els serveis socials municipals.
1. Garantir que el personal de l'administració podrà acompanyar a la ciutadania en la seva interacció digital amb l'administació a partir de programes de formació interna.

### Compra pública de dispositius electrònics i circularitat
> Faltat tot l'apartat

### Estandars lliures
> Faltat tot l'apartat
